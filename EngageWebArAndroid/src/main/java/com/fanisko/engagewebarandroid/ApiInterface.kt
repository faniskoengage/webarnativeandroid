package com.fanisko.engagewebarandroid

import retrofit2.http.GET
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiInterface {

    @POST(value = "validation/")
    fun getData(@Body dataModal: AuthBody?): Call<AuthResponse>

}