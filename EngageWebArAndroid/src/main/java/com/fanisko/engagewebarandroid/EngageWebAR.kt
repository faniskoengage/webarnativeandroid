package com.fanisko.engagewebarandroid

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class EngageWebAR {
    fun launchEngageWebAR(context: Context?, matchId: String?, accessKey: String?, secretKey: String?) {

        val intent = Intent(context, EngageWebARActivity::class.java)
        intent.putExtra("matchId", matchId)
        context?.startActivity(intent)

//        val authUrl = "https://webview.fanisko.com/"
//
//        val retrofitBuilder = Retrofit.Builder()
//            .addConverterFactory(GsonConverterFactory.create())
//            .baseUrl(authUrl)
//            .build()
//            .create(ApiInterface::class.java)
//
//        val authBody: AuthBody = AuthBody(accessKey, secretKey)
//
//        val retrofitData = retrofitBuilder.getData(authBody)
//
//        retrofitData.enqueue(object : Callback<AuthResponse?> {
//            override fun onResponse(call: Call<AuthResponse?>, response: Response<AuthResponse?>) {
//                var isSuccess: Int = 0
//                isSuccess = response.body()?.success ?:  0;
//
//                if( isSuccess == 1){
//
//                    val intent = Intent(context, EngageWebARActivity::class.java)
//                    intent.putExtra("matchId", matchId)
//                    context?.startActivity(intent)
//
//
//                }
//                else{
//                    showAlert(context)
//                }
//
//            }
//
//            override fun onFailure(call: Call<AuthResponse?>, t: Throwable) {
//                showAlert(context)
//            }
//        })

    }

    fun showAlert(context: Context?) {

        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setMessage("Access Denied...")
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                //do things
            })
        val alert: AlertDialog = builder.create()
        alert.show()

    }
}