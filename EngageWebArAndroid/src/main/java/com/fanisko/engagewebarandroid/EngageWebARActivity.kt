package com.fanisko.engagewebarandroid

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.webkit.JsResult
import android.webkit.PermissionRequest
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.fanisko.engagewebarandroid.databinding.ActivityEngageWebAractivityBinding

class EngageWebARActivity : AppCompatActivity() {

    private val TAG = "EngageWebArActivity"
    private val CAMERA_REQUEST_CODE = 2000
    private lateinit var binding: ActivityEngageWebAractivityBinding
    var matchId = ""
    //var arBaseUrl = "https://icc.onrender.com"
//    var arBaseUrl = "https://iccwebar.fanisko.com:1234"
    var arBaseUrl = "https://webar.fanisko.com/index.html"
    var isSettingsNavigate: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEngageWebAractivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        matchId = intent.extras?.getString("matchId").toString()

        setupPermissions()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true)
        }

        binding.webView.settings.apply {
            allowContentAccess = true
            javaScriptEnabled = true
            domStorageEnabled = true;
            allowFileAccess = true;
            allowContentAccess = true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                mediaPlaybackRequiresUserGesture = false
            }
        }

        binding.webView.settings.builtInZoomControls = true
        binding.webView.settings.displayZoomControls = false
        binding.webView.webViewClient = WebViewClient()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.webView.webChromeClient = object : WebChromeClient() {
                override fun onPermissionRequest(request: PermissionRequest) {
                    runOnUiThread {
                        if (request.resources.first() == PermissionRequest.RESOURCE_VIDEO_CAPTURE) {
                            request.grant(request.resources)
                        } else {
                            request.deny()
                        }
                    }
                }
                override fun onJsBeforeUnload(view: WebView?, url: String?, message: String?, result: JsResult): Boolean {
                    result.confirm()
                    return true
                }
            }
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
        }

        binding.permissionButton.setOnClickListener {
            //makeRequest()
            isSettingsNavigate = true;
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            binding.webView.visibility = View.GONE
            binding.permissionLayout.visibility = View.VISIBLE
        } else {
            if(isSettingsNavigate){
                isSettingsNavigate=false
                setupWebView()
            }
        }

    }

    private fun setupWebView() {
        binding.webView.visibility = View.VISIBLE
        binding.permissionLayout.visibility = View.GONE
        binding.webView.loadUrl("$arBaseUrl?match_id=$matchId")
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission missing")
            makeRequest()
        } else {
            setupWebView()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.CAMERA),
            CAMERA_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")
                    binding.webView.visibility = View.GONE
                    binding.permissionLayout.visibility = View.VISIBLE
                    binding.permissionButton.visibility = View.VISIBLE
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                    setupWebView()
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack()
        } else {
            finish()
        }
        return true
    }
    override fun onBackPressed() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack()
        } else {
            super.onBackPressed()
        }
    }
}