package com.fanisko.engagewebarandroid

data class AuthBody(
    val access_key: String?,
    val secret_key: String?
)