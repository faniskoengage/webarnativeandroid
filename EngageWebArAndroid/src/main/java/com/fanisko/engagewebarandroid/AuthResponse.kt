package com.fanisko.engagewebarandroid

data class AuthResponse(
    val message: String,
    val success: Int
)